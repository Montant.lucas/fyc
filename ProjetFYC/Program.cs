using Microsoft.EntityFrameworkCore;
using ProjetFYC.DbModel;
using ProjetFYC.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Data.Entity;

var builder = WebApplication.CreateBuilder(args);
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Pour ajouter le contexte de base de donn�es
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseLazyLoadingProxies().UseNpgsql(connectionString));

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.MapGet("/vignerons", (AppDbContext dbcontext) => dbcontext.Vignerons);

app.MapGet("/caves", (AppDbContext dbcontext) => dbcontext.Caves);

app.Run();

//    EntityFrameworkCore\Add-Migration first-migration
//    EntityFrameworkCore\Update-Database
