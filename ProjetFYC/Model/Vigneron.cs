﻿namespace ProjetFYC.Model
{
    public class Vigneron
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
