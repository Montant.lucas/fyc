﻿namespace ProjetFYC.Model
{
    public class Cave
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public int VigneronId { get; set; }
        public virtual Vigneron Vigneron { get; set; }
    }
}
