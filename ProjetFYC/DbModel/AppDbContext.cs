﻿using Microsoft.EntityFrameworkCore;
//Appel du dossier model pour récuperer nos models 
using ProjetFYC.Model;

namespace ProjetFYC.DbModel
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        //ici on set la listes des models pouvant etre ajouter en base de données
        public DbSet<Vigneron> Vignerons => Set<Vigneron>();
        public DbSet<Cave> Caves => Set<Cave>();

    }
}
